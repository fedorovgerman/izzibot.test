@extends('layouts.app')

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div>
        {{ Form::open(['action' => 'GuestbookController@store', 'name' => 'addMessage']) }}
        <div class="form-group">
            {{ Form::label('user_name', 'Имя') }}
            {{ Form::text('user_name', $message->name ?? null,
                [
                    'placeholder' => 'Имя',
                    'class' => 'form-control',
                    'required',
                    //'pattern' => '[A-Za-z0-9]*',
                    //'oninvalid' => "setCustomValidity('Разрешены только буквы латинского алфавита и цифры')",
                ]
            )}}
        </div>
        <div class="form-group">
            {{ Form::label('user_email', 'E-mail') }}
            {{ Form::email('user_email', $message->alias ?? null , ['placeholder' => 'email', 'class' => 'form-control','required']) }}
        </div>
        <div class="form-group">
            {{ Form::label('user_homepage', 'E-mail') }}
            {{ Form::text('user_homepage', $message->alias ?? null , ['placeholder' => 'Домашняя страница', 'class' => 'form-control']) }}
        </div>
        <div class="form-group">
            {{ Form::label('user_text', 'Текст') }}
            {{ Form::textarea('user_text', $message->text ?? null , ['class' => 'form-control','required']) }}
        </div>
        {{ Form::submit('Добавить', ['class' => 'btn btn-primary']) }}
    </div>
    <hr>
    <div>
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">{{ link_to_route('guestbook', $title = '#', $parameters= ['sortBy' => 'id']) }}</th>
                <th scope="col">{{ link_to_route('guestbook', $title = 'Name', $parameters= ['sortBy' => 'user_name']) }}</th>
                <th scope="col">{{ link_to_route('guestbook', $title = 'Email', $parameters= ['sortBy' => 'user_email']) }}</th>
                <th scope="col">{{ link_to_route('guestbook', $title = 'Homepage', $parameters= ['sortBy' => 'user_homepage']) }}</th>
                <th scope="col">{{ link_to_route('guestbook', $title = 'Text', $parameters= ['sortBy' => 'user_text']) }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($messages as $message)
            <tr>
                <th scope="row">{{ $loop->index+1 }}</th>
                <td>{{ $message->user_name }}</td>
                <td>{{ $message->user_email }}</td>
                <td>{{ $message->user_homepage }}</td>
                <td>{{ $message->user_text }}</td>
            </tr>
            @endforeach
            </tbody>
        </table>
        {{ $messages->links() }}
    </div>
@endsection