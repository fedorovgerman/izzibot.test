<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guestbook extends Model
{
    protected $fillable = ['user_name', 'user_email', 'user_homepage', 'user_text', 'user_ip', 'user_agent'];
}
