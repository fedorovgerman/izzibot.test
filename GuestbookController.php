<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Guestbook;
use Illuminate\Support\Facades\Input;
use Cookie;

class GuestbookController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        $sortBy = Input::get('sortBy') ?? 'id';
        $messages = Guestbook::orderBy($sortBy, 'desc')->paginate(3);
        return view('guestbook', compact('messages'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request) {
        $request->validate([
            'user_name'  => 'required|unique:guestbooks|regex:/^[A-Za-z0-9]*$/i',
            'user_email' => 'required|email',
            'user_text'  => 'required'
        ]);
        $guestBook = new Guestbook();
        $request->request->add([
            'user_agent' => $request->userAgent(),
            'user_ip'    => $request->ip(),
            'user_text'  => strip_tags($request->user_text)
        ]);
        if ($guestBook->fill($request->all())->save()) {
            return redirect()->back()->with('status', 'success')->with('message', 'Сообщение успешно добавлено');
        }
    }
}
